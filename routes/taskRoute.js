/*const express = require("express");
// The "taskController" allow us to use the function defined inside it.
const taskController = require("../controllers/taskController")

// Allows access to hhtp method middlewares that makes it easier to create  routes for our aplications.
const router = express.Router();

// Route to get all tasks
router.get("/", (req,res) => {

	taskController.getAllTasks().then(tasks => res.send(tasks));
})

// Route to Create a Task
router.post("/", (req, res) => {
	// The "createTask" function needs data from the request body, so we need this t supply in the taskController.createTask(arg)
	taskController.createTask(req.body).then(task => res.send(task));
})

// Use "module.exports" to export the router object to be used in the server
module.exports = router;*/

const express = require("express");
// The "taskController" allow us to use the function defined inside it.
const taskController = require("../controllers/taskController");

// Allows access to HTTP Method middlewares that makes it easier to create routes for our application.
const router = express.Router();

// Route to get all tasks
router.get("/", (req, res)=>{

	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
})

// Route to create a Task
// localhost:3001/task it is the same with localhost:3001/task/
router.post("/", (req, res)=>{
	// The "createTask" function needs data from the request body, so we need it to supply in the taskController.createTask(argument).
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
})

// Delete
// colon (:) is an identifie that helps create a dynamic route which allow us to supply information from the url
// ":id" is a wildcard where you can input the objectId as a value
// Ex localhost:3001/tasks/:id 
router.delete("/:id", (req, res) => {
	// if inormation will be coming from the URL, the data can be accessed from the request "params" property
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController))

})

router.patch("/:id", (req, res) =>{
	taskController.updateTask(req.params.id,req.body).then(resultFromController => res.send(resultFromController));
})


// // Route to get specific task
router.get("/:id", (req, res)=>{

	taskController.getSpecificTasks(req.params.id).then(resultFromController => res.send(resultFromController));
})



router.put("/:id/complete", (req, res) =>{
	taskController.updateTaskStatus(req.params.id).then(resultFromController => res.send(resultFromController));
})

// Use "module.exports" to export the router object to be use in the server
module.exports = router;


